﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Operation
    {
        public int OperationID { get; set; }
        public string OperationDefinition { get; set; }
    }
}
