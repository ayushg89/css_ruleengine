﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Threshold
    {
        public int ThresholdID { get; set; }
        public int OperationID { get; set; }
        public string ComparisonOperator { get; set; }
        public double ComparisonResult { get; set; }

    }
}
