﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Data
    {
        public int ID { get; set; }
        public int VariableA { get; set; }
        public int VariableB { get; set; }
        public int VariableC { get; set; }
        public int VariableD { get; set; }
    }
}
