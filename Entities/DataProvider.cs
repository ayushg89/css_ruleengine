﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class DataProvider
    {
        public List<Data> GetData()
        {
            List<Data> data = new List<Data>()
            {
                new Data
                {
                    ID = 1956738,
                    VariableA = 20,
                    VariableB = 33,
                    VariableC = 44,
                    VariableD = 3
                },
                new Data
                {
                    ID = 1956739,
                    VariableA = 28,
                    VariableB = 35,
                    VariableC = 16,
                    VariableD = 33
                },
                new Data
                {
                    ID = 1956740,
                    VariableA = 320,
                    VariableB = 3,
                    VariableC = 46,
                    VariableD = 0
                }
            };
            return data;
        }

        public List<Operation> GetOperations()
        {
            List<Operation> operations = new List<Operation>()
            {
                new Operation
                {
                    OperationID=1,
                    OperationDefinition="VariableA+VariableB"
                },
                new Operation
                {
                    OperationID=2,
                    OperationDefinition="VariableA-VariableB"
                },
                new Operation
                {
                    OperationID=3,
                    OperationDefinition="VariableA/VariableB"
                }
            };

            return operations;
        }

        public List<Threshold> GetThresholds()
        {
            List<Threshold> thresholds = new List<Threshold>()
            {
                new Threshold
                {
                    ThresholdID=1,
                    OperationID=1,
                    ComparisonOperator="<",
                    ComparisonResult=100
                },
                new Threshold
                {
                    ThresholdID=2,
                    OperationID=1,
                    ComparisonOperator=">=",
                    ComparisonResult=2
                },
                new Threshold
                {
                    ThresholdID=3,
                    OperationID=2,
                    ComparisonOperator="<",
                    ComparisonResult=0.1
                },
                new Threshold
                {
                    ThresholdID=1,
                    OperationID=3,
                    ComparisonOperator="==",
                    ComparisonResult=1
                }
            };
            return thresholds;
        }
    }
}