﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
   public  class OperationResult
    {
        public int DataID { get; set; }
        public int OperationID { get; set; }
        public double Result { get; set; }
    }
}
