﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class EngineResult
    {
        public int DataID { get; set; }
        public int OperationID { get; set; }
        public int ThresholdID { get; set; }
        public string Comparison { get; set; }
        public bool Result { get; set; }
    }
}

