﻿using System;
using System.Collections.Generic;
using System.Text;
using Contracts;
using Entities;

namespace Repository
{
    public class Operation : IOperation
    {
        private IThreshold _threshold;

        public Operation(IThreshold theThreshold)
        {
            _threshold = theThreshold;
        }
        public List<OperationResult> CalculateOperations(List<Entities.Operation> operation)
        {
            List<Data> rawData = new List<Data>();
            DataProvider data = new DataProvider();
            rawData = data.GetData();
            List<Entities.Operation> preDefineOperation = new List<Entities.Operation>();
            preDefineOperation = data.GetOperations();

            List<OperationResult> result = new List<OperationResult>();
            foreach (var ops in preDefineOperation)
            {
                int opsId = ops.OperationID;
                string[] strSeparator = {"+", "-", "*", "/"};
                string[] operands =
                    ops.OperationDefinition.Split(strSeparator, 2, StringSplitOptions.RemoveEmptyEntries);
                string LeftOperand = operands[0];
                string rightOpearand = operands[1];
                string strOperator = "";
                if (ops.OperationDefinition.Contains("+"))
                    strOperator = "+";
                else if (ops.OperationDefinition.Contains("-"))
                    strOperator = "-";
                else if (ops.OperationDefinition.Contains("*"))
                    strOperator = "*";
                else if (ops.OperationDefinition.Contains("/"))
                    strOperator = "/";
                foreach (var item in rawData)
                {
                    switch (strOperator)
                    {
                        case "+":
                            result.Add(new OperationResult
                            {
                                DataID = item.ID, OperationID = opsId,
                                Result = Convert.ToInt32(item.GetType().GetProperty(LeftOperand).GetValue(item, null)) +
                                         Convert.ToInt32(item.GetType().GetProperty(rightOpearand).GetValue(item, null))
                            });
                            break;
                        case "-":
                            result.Add(new OperationResult
                            {
                                DataID = item.ID, OperationID = opsId,
                                Result = Convert.ToInt32(item.GetType().GetProperty(LeftOperand).GetValue(item, null)) -
                                         Convert.ToInt32(item.GetType().GetProperty(rightOpearand).GetValue(item, null))
                            });
                            break;
                        case "*":
                            result.Add(new OperationResult
                            {
                                DataID = item.ID, OperationID = opsId,
                                Result = Convert.ToInt32(item.GetType().GetProperty(LeftOperand).GetValue(item, null)) *
                                         Convert.ToInt32(item.GetType().GetProperty(rightOpearand).GetValue(item, null))
                            });
                            break;
                        case "/":
                            result.Add(new OperationResult
                            {
                                DataID = item.ID, OperationID = opsId,
                                Result = Convert.ToInt32(item.GetType().GetProperty(LeftOperand).GetValue(item, null)) /
                                         Convert.ToInt32(item.GetType().GetProperty(rightOpearand).GetValue(item, null))
                            });
                            break;
                    }

                }
            }

            return result;
            //return View();

        }
    }
}
