﻿using System;
using System.Collections.Generic;
using System.Text;
using Contracts;
using Entities;

namespace Repository
{
    public class Threshold : IThreshold
    {
       
        public List<EngineResult> CalculateThreshold(List<OperationResult> opsResult)
        {
            
            List<Entities.Threshold> thresholds = new List<Entities.Threshold>();
            DataProvider data = new DataProvider();
            thresholds = data.GetThresholds();
            
            List<EngineResult> engineResult = new List<EngineResult>();
            foreach (var item in thresholds)
            {
                foreach (var opResult in opsResult)
                {
                    if (Compare(item.ComparisonOperator, opResult.Result, item.ComparisonResult)) //opResult.Result == item.ComparisonResult
                    {
                        engineResult.Add(new EngineResult
                        {
                            DataID = opResult.DataID,
                            OperationID = opResult.OperationID,
                            ThresholdID = item.ThresholdID,
                            Comparison = opResult.Result + item.ComparisonOperator + item.ComparisonResult,
                            Result = true
                        });
                    }
                    else
                    {
                        engineResult.Add(new EngineResult
                        {
                            DataID = opResult.DataID,
                            OperationID = opResult.OperationID,
                            ThresholdID = item.ThresholdID,
                            Comparison = opResult.Result + item.ComparisonOperator + item.ComparisonResult,
                            Result = false
                        });
                    }
                }
            }
            return engineResult;
        }

        public static bool Compare<T>(string op, T left, T right) where T : IComparable<T>
        {
            switch (op)
            {
                case "<": return left.CompareTo(right) < 0;
                case ">": return left.CompareTo(right) > 0;
                case "<=": return left.CompareTo(right) <= 0;
                case ">=": return left.CompareTo(right) >= 0;
                case "==": return left.Equals(right);
                case "!=": return !left.Equals(right);
                default: throw new ArgumentException("Invalid comparison operator: {0}", op);
            }
        }
    }
}
