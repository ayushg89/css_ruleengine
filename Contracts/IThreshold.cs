﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IThreshold
    {
        List<EngineResult> CalculateThreshold(List<OperationResult> results);
    }

   
}
