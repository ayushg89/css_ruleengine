﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;

namespace Contracts
{
    public interface IOperation
    {

        List<OperationResult> CalculateOperations(List<Operation> operation);
       
    }

    
}
